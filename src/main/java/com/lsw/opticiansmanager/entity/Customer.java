package com.lsw.opticiansmanager.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String customerName;

    @Column(nullable = false, length = 20)
    private String customerPhone;
    @Column(nullable = false)
    private LocalDate dateLast;

    @Column(nullable = false)
    private Float figureEyeRight;

    @Column(nullable = false)
    private Float figureEyeLeft;




}
