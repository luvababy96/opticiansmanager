package com.lsw.opticiansmanager.repository;

import com.lsw.opticiansmanager.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer,Long> {
}
